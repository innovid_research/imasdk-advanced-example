function _InnovidAds() as Object
  instance_ = m["_.InnovidAds"]

  if instance_ = invalid then
    instance_ = {
      _ : m,
      _irollById : {},
      _truexHandler: invalid,
      _truexEvents:  {
        exitWithSkipAdBreak : true,
        exitWithAdFree: true,
        exitAutoWatch: true,
        exitSelectWatch: true,
        exitBeforeOptIn: true
      },
      _playbackHandler : invalid,

      init : function(playbackHandler_, truexHandler_ = invalid) as Void
        m._playbackHandler = playbackHandler_
        m._truexHandler = truexHandler_

        ' override RAF's factory method
        Roku_Ads().util.config.getIrollFactory = function() as Object
          return _InnovidAds()
        end function
      end function,

      ' this method will invoked by RAF
      create : function(adTag_ as String, adInfo_ as Object, listener_ = invalid, _ = invalid) as Object
        if adInfo_.LookupCI("started") = invalid then
          adInfo_.started = false
        end if

        if adInfo_.LookupCI("viewed") = invalid then
          adInfo_.viewed = false
        end if

        if adInfo_.started and adInfo_.viewed then
          return {
            show : function(player_ = Invalid as Object) as Void
              return
            end function
          }
        end if

        wrapper_ = _InnovidAds_Iroll_Wrapper( adTag_, adInfo_, listener_ )
        m._irollById[ wrapper_.id ] = wrapper_

        return wrapper_
      end function,

      isIrollEvent : function(msg_ = invalid) as Boolean
        if msg_ = invalid or type( msg_ ) <> "roSGNodeEvent" then
          return false
        end if

        return (m._irollById.DoesExist(msg_.getNode()) and (msg_.getField() = "event" or msg_.getField() = "request"))
      end function,

      _isTrueXEvent : function(msg_ = invalid) as Boolean
        evt_ = msg_.getData()

        if not(m.util.isObject( evt_ )) or not(m.util.isString( evt_.type )) then
          return false
        end if

        return m._truexEvents.LookupCI( evt_.type ) <> invalid
      end function,

      _isPlaybackRequestEvent : function(msg_ as Object) as Boolean
        return msg_.getField() = "request"
      end function,

      ' Supported events:
      ' Impression       - Start of ad render (e.g., first frame of a video ad displayed)
      ' FirstQuartile    - 25% of video ad rendered
      ' Midpoint         - 50% of video ad rendered
      ' ThirdQuartile    - 75% of video ad rendered
      ' Complete         - 100% of video ad rendered
      ' Ended            - Ad ended
      ' AcceptInvitation - User launched another portion of an ad (for interactive ads)
      ' Error            - Error during ad rendering
      ' Expand           - the user activated a control to expand the creative.
      ' Collapse         - the user activated a control to reduce the creative to its original dimensions.
      ' Close            - User exited out of ad rendering before completion
      ' Skip             - User skipped ad (if skippable)
      ' Pause            - User paused ad
      ' Resume           - User resumed ad
      handle : function(msg_ as Object) as Boolean
        irollEvt_ = msg_.getdata()
        wrapper_ = m._getWrapper(msg_.getRoSGNode())

        ? " >>> InnovidAds # handle( ";irollEvt_.type;" )"

        if m._isTrueXEvent( msg_ ) then
          m._processTrueXEvent( msg_ )
        else if m._isPlaybackRequestEvent( msg_ ) then
          m._procesPlaybackRequest( msg_ )
        else
          if irollEvt_.type = "Ended" then
            m._unloadAd( wrapper_ )
          end if

          rafEvt_ = { type : irollEvt_.type }

          ' RAF's way to handle expanded state
          if irollEvt_.type = "Expand" then
            rafEvt_.type = "Impression"
            rafEvt_.companion = true
          else if irollEvt_.type = "Collapse" then
            rafEvt_.type = "Close"
            rafEvt_.companion = true
          end if

          Roku_Ads().fireTrackingEvents( wrapper_.ad, rafEvt_ )
        end if
      end function,

      _getWrapper : function(node_ as Object) as Object
        return m._irollById[ node_.id ]
      end function,

      _unloadAd : function(wrapper_ as Object, reason_ = invalid) as Void
        if wrapper_ = invalid then
          return
        end if

        ' unregister
        m._irollById.Delete( wrapper_.id )
        ' unload
        wrapper_._unloadAd( reason_ )
      end function,

      _procesPlaybackRequest : function(msg_ as Object) as Void
        if m.util.isFunction( m._playbackHandler ) then
          callback_ = m._playbackHandler
          callback_( msg_ )
        end if
      end function,

      _processTrueXEvent : function(msg_ as Object) as Void
        if not(irollEvt_.type = "CBA_Skip_Card_Started") then
          m._unloadAd( wrapper_ )
        end if

        if m.util.isFunction( m._truexHandler ) then
          handler_ = m._truexHandler
          handler_( wrapper_.ad, irollEvt_ )
        end if
      end function,

      util : {
        isBoolean : function(value_ as Dynamic) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifBoolean") <> invalid
        end function,

        isArray : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifArray") <> invalid
        end function,

        isString : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifString") <> invalid
        end function,

        isObject : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifAssociativeArray") <> invalid
        end function,

        isNumeric : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and (GetInterface(value_, "ifInt") <> invalid or GetInterface(value_, "ifFloat") <> invalid)
        end function,

        isInteger : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifInt") <> invalid
        end function,

        isFloat : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifFloat") <> invalid
        end function

        isScalar : function(value_) as Boolean
          return m.isNumeric( value_ ) or m.isBoolean( value_ ) or m.isString( value_ )
        end function,

        isFunction : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifFunction") <> invalid
        end function,

        isEnumerable : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and GetInterface(value_, "ifEnum") <> invalid
        end function,

        isSGNode : function(value_) as Boolean
          return type(value_) <> "<uninitialized>" and type( value_ ) = "roSGNode"
        end function
      }
    }

    m["_.InnovidAds"] = instance_
  end if

  return instance_
end function

function _InnovidAds_Iroll_Wrapper(adTag_ as String, adInfo_ as Object, listener_ = invalid) as Object
  return {
    id       : "iroll-" + CreateObject("roDeviceInfo").GetRandomUUID(),
    _        : m,
    tag      : adTag_,
    ad       : adInfo_,
    listener : listener_,
    util     : _InnovidAds().util,

    eventLoopRunning : false    ' @raf @interface
    mode             : invalid  ' @raf @interface "exited" or "completed"
    extTracking      : adInfo_, ' @raf @interface
    userExited       : false,   ' @raf @interface
    video            : invalid, ' @raf @interface video sgnode
    player           : invalid, ' @raf @interface raf player
    port             : invalid, ' @raf @interface
    canvas           : invalid, ' @raf @interface

    instance    : invalid, ' iroll renderer instance
    container   : invalid, ' parent

    show : function(player_ = invalid) as Void
      ? " >>> InnovidAd # show()"
      ? " -- canvas: ";player_.canvas.instance.subtype();", parent ":player_.canvas.instance.getparent().subtype()
      ? " -- player: ";player_.player.subtype();", parent: ";player_.player.getparent().subtype()

      m.player    = player_
      m.video     = player_.player
      m.container = player_.canvas.instance
      m.canvas    = player_.canvas
      m.port      = player_.port

      m._loadAd()
      m._startAd()
    end function,

    unload : function(disposeVideo_ = true) as Void
      ? " >>> InnovidAd # unload(disposeVideo: ";disposeVideo_;")"
      m._unloadAd()
    end function,

    injectMessage : function(msg_ as Object) as Boolean
      wrappedMsg_ = invalid

      if m.util.isObject( msg_ ) and type( msg_.msg ) = "roSGNodeEvent" then
        wrappedMsg_ = msg_.msg
        field_ = wrappedMsg_.getField()

        ? " >>> InnovidAd # injectMessage( ";field_;

        if field_ = "position" or field_ = "state" or field_ = "bufferingStatus" then
          ? ", ";wrappedMsg_.getNode();".";field_;": ";wrappedMsg_.getData();" )"
        else
          ? " )"
        end if

        if wrappedMsg_.getfield() = "closed" then
          return false
        end if
      else
        ' ? " >>> InnovidAd # injectMessage() -- ";msg_
      end if

      if not(m.util.isObject( msg_ )) or m.instance = invalid then
        return false
      end if

      handled_ = false
      isVideoEvt_ = Roku_Ads().util.isVideoMsg( msg_ )
      isCanvasEvt_ = Roku_Ads().util.isCanvasMsg( msg_ )
      isVideoCompletedEvt_ = (m.util.isObject( msg_ ) and m.util.isString( msg_.type ) and LCase( msg_.type ) = "completed")

      if (isVideoEvt_ or isVideoCompletedEvt_) then
        m.instance.action = {
          type : "notifyPlaybackStateChanged",
          position: m.player.player.position,
          state: m.player.player.state
        }

        handled_ = true
      else if isCanvasEvt_ then
        if m.util.isObject( m.listener ) and m.util.isFunction( m.listener.handleRemoteKeyPressed ) then
          result_ = m.listener.handleRemoteKeyPressed( msg_, m )
          if (m.util.isObject( result_ ) and m.util.isBoolean( result_.doExit ) and result_.doExit) then
            m.mode = "exited"     ' @raf !important
            m.userExited = true   ' @raf !important

            _InnovidAds()._unloadAd( m, "userExited" )
          end if
        end if

        handled_ = true
      end if

      return handled_
    end function,

    _unloadAd : function(reason_ = invalid) as Void
      ? ""
      ? " >>> InnovidAd # _unloadAd(reason: ";reason_;") -- ";m.instance
      ? ""

      ' unload instance
      if m.instance <> invalid then
          m.instance.action = { type : "unload", reason : reason_ }

          if m.instance.getParent() <> invalid then
            m.instance.getParent().removeChild( m.instance )
        end if

        m.instance.unobserveField("event")
      end if

      ' reset values
      m.instance = invalid

      ' return focus to parent container
      if m.container <> invalid then
        m.container.setFocus( true )
      end if
    end function,

    _loadAd : function() as Void
      ? " >>> InnovidAd # _loadAd() -- ";m.ad

      screen_ = Roku_Ads_getSGDisplaySize()

      m.instance = m.container.createChild("InnovidAds:DefaultIrollAd")
      m.instance.id = m.id
      m.instance.observeFieldScoped("event", m.player.port)
      m.instance.observeFieldScoped("request", m.player.port)
      m.instance.action = {
        type   : "init",
        uri    : m.tag,
        ssai   : {
          adapter    : "default",
          renderTime : m.ad.renderTime,
          duration   : m.ad.duration
        },
        width  : screen_.w,
        height : screen_.h,
        params : m.ad
      }
    end function,

    _startAd : function() as Void
      ? " >>> InnovidAd # _startAd() -- ";m.instance
      ' start iroll
      m.instance.action = { type : "start" }
      ' add to stage
      m.container.appendChild( m.instance )
    end function
  }
end function
